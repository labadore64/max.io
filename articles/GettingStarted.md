# Using max.IO

``max.IO`` is a MaxLib Helper Library that reads and writes files on a specific part of the filesystem.

* [Installation](Installation.md)
* [File System](FileSystem.md)
* [File IO](FileSystem.md)

## Max Dependencies:

* [max.serialize](https://labadore64.gitlab.io/max.serialize/)
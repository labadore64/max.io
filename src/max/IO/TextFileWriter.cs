﻿using max.IO.model;
using System;
using System.IO;
using System.Text;

namespace max.IO
{
    /// <summary>
    /// This class writes to text files in the Max environment.
    /// </summary>
    public class TextFileWriter : FileWriter
    {


        #region File Manipulation

        /// <summary>
        /// Appends the contents to the end of a file.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Contents">Contents</param>
        public static void AppendFile(string Path, string Contents)
        {
            AppendFile(Path, Contents, null);
        }

        /// <summary>
        /// Appends the contents to the end of a file.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Contents">Contents</param>
        /// <param name="Encoding">Encoding</param>
        public static void AppendFile(string Path, string Contents, Encoding Encoding)
        {
            CreateDirectory(Path);
            if (Encoding == null)
            {
                File.AppendAllText(Path, Contents, Encoding);
            } else
            {
                File.AppendAllText(Path, Contents);
            }
        }

        /// <summary>
        /// Appends the contents to the end of the file.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <param name="Contents">Contents</param>
        public static void AppendFile(string Location, string Filename, string Extension, string Contents)
        {

            AppendFile(GetPath(Location, Filename, Extension), Contents);
        }

        /// <summary>
        /// Appends the contents to the end of the file.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <param name="Encoding">File encoding</param>
        /// <param name="Contents">Contents</param>
        public static void AppendFile(string Location, string Filename, string Extension, string Contents, Encoding Encoding)
        {
            AppendFile(GetPath(Location, Filename, Extension), Contents, Encoding);
        }

        /// <summary>
        /// Write the contents to the file, overwriting anything already in it.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <param name="Contents">Contents</param>
        public static void WriteFile(string Location, string Filename, string Extension, string Contents)
        {
            WriteFile(GetPath(Location, Filename, Extension), Contents);
        }

        /// <summary>
        /// Write the contents to the file, overwriting anything already in it.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <param name="Encoding">File encoding</param>
        /// <param name="Contents">Contents</param>
        public static void WriteFile(string Location, string Filename, string Extension, string Contents, Encoding Encoding)
        {
            WriteFile(GetPath(Location, Filename, Extension), Contents,Encoding);
        }

        /// <summary>
        /// Write the contents to the file, overwriting anything already in it.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Contents">Contents</param>
        public static void WriteFile(string Path, string Contents)
        {
            WriteFile(Path, Contents, null);
        }

        /// <summary>
        /// Write the contents to the file, overwriting anything already in it.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Contents">Contents</param>
        /// <param name="Encoding">Encoding</param>
        public static void WriteFile(string Path, string Contents, Encoding Encoding)
        {

            CreateDirectory(Path);
            try
            {
                if (Encoding == null)
                {
                    File.WriteAllText(Path, Contents);
                }
                else
                {
                    File.WriteAllText(Path, Contents, Encoding);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion
    }
}

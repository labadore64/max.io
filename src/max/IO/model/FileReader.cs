﻿using max.util;
using System.IO;

namespace max.IO.model
{
    /// <summary>
    /// This class represents the template for a File Reader.
    /// </summary>
    public abstract class FileReader
    {
        /// <summary>
        /// Gets the path of where a file can be found in the max environment.
        /// </summary>
        /// <param name="Location">Directory</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <returns>Directory path</returns>
        protected static string GetPath(string Location, string Filename, string Extension)
        {
            // correct paths
            Location = StringUtil.StripString(Location);
            Filename = StringUtil.StripString(Filename);
            Extension = StringUtil.StripString(Extension);

            // first, test if the path exists in the app directory. if not, test if it exists in the maxlib directory. If neither, just
            // combine the first two paths.
            string DefaultPath = IOProperties.AppDirectoryPath + "\\" + Path.Combine(Location, Filename + "." + Extension);
            string BackupPath = DefaultPath;

            // If the IOProperties app directory isn't set to the default directory ("MaxLib") it
            // will set the backup path to the default directory. This way, the app directory's files
            // will always override the defaults, but if they aren't found, it can rely on the defaults
            // as backups (or use the defaults as a standard).
            if (IOProperties.DEFAULT_APPDATA_DIRECTORY != IOProperties.AppDirectory)
            {
                BackupPath = IOProperties.DefaultDirectoryPath + "\\" + Path.Combine(Location, Filename + "." + Extension);
            }

            // If the default path exists

            if (File.Exists(DefaultPath))
            {
                // return the default path.
                return DefaultPath;

            }
            // else If the backup path exists
            else if (File.Exists(BackupPath))
            {
                // return the backup path
                return BackupPath;
            }

            // If neither is found, return the default path. Note that this will throw file not found errors.

            return DefaultPath;
        }
    }
}

﻿using max.util;
using System.IO;

namespace max.IO.model
{
    /// <summary>
    /// This class represents the template for a File Writer.
    /// </summary>
    public abstract class FileWriter
    {
        /// <summary>
        /// This gets the file location relative to the current app environment.
        /// </summary>
        /// <param name="Location">Location of the file</param>
        /// <param name="Filename">Filename of the file</param>
        /// <param name="Extension">File extension of the file</param>
        /// <returns>File path</returns>
        protected static string GetPath(string Location, string Filename, string Extension)
        {
            return GetPath(
                StringUtil.StripString(Location) +
                "\\" + StringUtil.StripString(Filename) +
                "." + StringUtil.StripString(Extension));
        }

        /// <summary>
        /// This gets the file location relative to the current app environment.
        /// </summary>
        /// <param name="Location">Path</param>
        /// <returns>File path</returns>
        protected static string GetPath(string Location)
        {
            return IOProperties.AppDirectoryPath + "\\" + StringUtil.StripString(Location);
        }

        /// <summary>
        /// Creates a directory if it doesn't already exist.
        /// </summary>
        /// <param name="Path">Path of the directory</param>
        protected static void CreateDirectory(string Path)
        {
            Path = System.IO.Path.GetDirectoryName(Path);
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
            }
        }
    }
}

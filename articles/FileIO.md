# File IO

## Reading Files

File readers inherit the abstract class ``FileReader``.

### Text Files

You can read the content of text files relative to the path stored in ``IOPropreties.AppDirectory.``

For example, if our AppDirectory is named "TestGame":

```
String text = TextFileReader.ReadFile("test\\textfile.config");
```

In Windows, this will store the entire contents of ``%APPDATA%\TestGame\test\textfile.config`` or ``%APPDATA%\MaxLib\test\textfile.config``, depending on whether the former file was located or not.

You can also read a file by passing the parts of the path as different parameters.

```
String text = TextFileReader.ReadFile("test","textfile","config");
```

It's also possible to return the lines as a string array:

```
String[] Lines = TextFileReader.ReadFileLines("test","textfile","config");
```

## Writing Files

File writers inherit the abstract class ``FileWriter``.

### Text Files

You can write to text files relative to the path stored in ``IOPropreties.AppDirectory.``

For example, if our AppDirectory is named "TestGame":

```
TextFileWriter.WriteFile("test\\textfile.config",Text);
```

In Windows, this will overwrite the contents of ``%APPDATA%\TestGame\test\textfile.config`` with the text provided.

You can also write a file by passing the parts of the path as different parameters.

```
TextFileWriter.WriteFile("test","textfile","config",Text);
```

If you want to append to a file, use ``AppendFile`` instead:

```
TextFileWriter.AppendFile("test\\textfile.config",Text);
TextFileWriter.AppendFile("test","textfile","config",Text);
```
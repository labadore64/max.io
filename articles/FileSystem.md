# File System

In order to contain file operations for an app, MaxLib requires that all file operations occur in either the base MaxLib directory or in the directory of your app.

## Usage

It is important to set the application directory for max.io to be able to properly read or write the files in your application.

```
// Sets the AppDirectory to TestGame
IOProperties.AppDirectory = "TestGame";
```

When trying to read files, max.io will attempt to first read from the AppDirectory. If nothing is found, it will then try to read from the default MaxLib directory. If a file cannot be found here, an exception is thrown.

When designing your game, it is a good idea to only include configurations that are required specifically for your game, and rely on defaults for common features; this allows end users to customize their default configs and use them across games. This also means that every game's individual configurations override any MaxLib configurations that may already exist.

When trying to write files, max.io will attempt to write in the AppDirectory; if this fails, an exception will be thrown.

In addition, config files can have a default extension defined, so you don't need to include the extension when looking for these files.

```
// Sets the default file extension to .properties
IOProperties.AppDirectory = ".properties";
```

## Platform Specific Information

The location of this directory depends on your operating environment.

### Windows

The base directory for default MaxLib configurations is ``%APPDATA%\MaxLib``, while the current app directory is ``%APPDATA%\[AppDirectory]``.
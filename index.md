# max.IO

``max.IO`` is a MaxLib Helper Library that reads and writes files on a specific part of the filesystem.

* [Documentation](https://labadore64.gitlab.io/max.io/)
* [Source](https://gitlab.com/labadore64/max.io/)
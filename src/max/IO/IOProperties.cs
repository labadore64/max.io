﻿using System;
using System.IO;

namespace max.IO
{
    /// <summary>
    /// These properties are used as defaults throughout other Max libraries.
    /// You can override them if you need to (for example, you may want to have
    /// your own game directory for some parts of the game, but use the MaxLib
    /// directory for others).
    /// </summary>
    public static class IOProperties
    {
        /// <summary>
        /// The default file extention for configs.
        /// </summary>
        /// <value>File extension (with period)</value>
        public static string FileExtension { get; set; } = ".config";

        /// <summary>
        /// The default AppData directory for Max libraries.
        /// This directory should be used for cross-compatible configurations,
        /// such as building engines, while a custom app directory should be used for
        /// game-specific configs.
        /// </summary>
        /// <value>Directory name</value>
        public const string DEFAULT_APPDATA_DIRECTORY = "MaxLib";

        /// <summary>
        /// The app directory for this specific app. Defaults to MaxLib, but
        /// you can override this with your app's name.
        /// </summary>
        /// <value>Directory name</value>
        public static string AppDirectory { get; set; } = DEFAULT_APPDATA_DIRECTORY;

        /// <summary>
        /// The directory of the app file environment.
        /// </summary>
        /// <value>Path</value>
        public static string AppDirectoryPath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), AppDirectory);
            }
        }

        /// <summary>
        /// The directory of the default Max file environment.
        /// </summary>
        /// <value>Path</value>
        public static string DefaultDirectoryPath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), DEFAULT_APPDATA_DIRECTORY);
            }
        }
    }
}

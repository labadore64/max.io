﻿using max.IO.model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace max.IO
{
    /// <summary>
    /// This class reads text files and returns their contents as strings in the Max environment.
    /// </summary>
    public class TextFileReader : FileReader
    {
        /// <summary>
        /// Returns all the text from a file as a string.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Encoding">File encoding</param>
        /// <returns>File text</returns>
        public static string ReadFile(string Path, Encoding Encoding)
        {
            try
            {
                if (File.Exists(Path))
                {
                    if(Encoding == null)
                    {
                        return File.ReadAllText(Path);
                    }
                    return File.ReadAllText(Path, Encoding);
                }
            } catch (Exception e)
            {
                throw e;
            }
            return "";
        }

        /// <summary>
        /// Returns all the text from a file as a string.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <returns>File text</returns>
        public static string ReadFile(string Path)
        {
            return ReadFile(Path, null);
        }

        /// <summary>
        /// Returns all the text from a file as a string.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <returns>File text</returns>
        public static string ReadFile(string Location, string Filename, string Extension)
        {
            return ReadFile(GetPath(Location, Filename, Extension));
        }

        /// <summary>
        /// Returns all the text from a file as a string.
        /// Files are stored in %appdata% on Windows.
        /// First tests the app directory for the file,
        /// then tests the default MaxLib directory ("MaxLib").
        /// </summary>
        /// <param name="Location">Directory of the file</param>
        /// <param name="Filename">Filename</param>
        /// <param name="Extension">File extension</param>
        /// <param name="Encoding">File encoding</param>
        /// <returns>File text</returns>
        public static string ReadFile(string Location, string Filename, string Extension, Encoding Encoding)
        {
            return ReadFile(GetPath(Location, Filename, Extension),Encoding);
        }

        /// <summary>
        /// Returns all the lines of the file as a string array.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <returns>String array of lines</returns>
        public static string[] ReadFileLines(string Path)
        {
            return ReadFileLines(Path,(Encoding)null);
        }

        /// <summary>
        /// Returns all the lines of the file as a string array.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="Encoding">File encoding</param>
        /// <returns>String array of lines</returns>
        public static string[] ReadFileLines(string Path, Encoding Encoding)
        {
            try
            {
                if (File.Exists(Path))
                {
                    if (Encoding == null)
                    {
                        return File.ReadAllLines(Path);
                    }
                    return File.ReadAllLines(Path, Encoding);
                }
            }
            catch (Exception e)
            {
                throw e;
            }


            return new string[0];
        }

        /// <summary>
        /// Returns the lines in the file between the startLine and endLine inclusive.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="startLine">Line to start</param>
        /// <param name="endLine">Line to end</param>
        /// <returns>String array of lines</returns>
        public static string[] ReadFileLines(string Path, int startLine, int endLine)
        {
            return ReadFileLines(Path, new int[][] { new int[] { startLine, endLine } }, null);
        }

        /// <summary>
        /// Returns the lines between a set of start and end lines.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="LineNumbers">An array of int[] where int[0] is the start line and int[1] is the end line.</param>
        /// <returns>String array of lines</returns>
        public static string[] ReadFileLines(string Path, int[][] LineNumbers)
        {
            return ReadFileLines(Path, LineNumbers, null);
        }

        /// <summary>
        /// Returns the lines between a set of start and end lines.
        /// </summary>
        /// <param name="Path">Path to file</param>
        /// <param name="LineNumbers">An array of int[] where int[0] is the start line and int[1] is the end line.</param>
        /// <param name="Encoding">File encoding</param>
        /// <returns>String array of lines</returns>
        public static string[] ReadFileLines(string Path, int[][] LineNumbers, Encoding Encoding)
        {
            List<string> lines = new List<string>();

            // initialize line counter
            int lineCounter = 0;

            // if there are line numbers to search thru
            if (LineNumbers.Length > 0)
            {
                try
                {
                    if (File.Exists(Path))
                    {
                        // if there is only one definition, do a special case
                        // that only checks the first index of the LineNumbers array
                        if (LineNumbers.Length == 1)
                        {
                            // creates an enumerable with the lines in the file
                            IEnumerable<string> Lines = GetEnumerable(Path, Encoding);

                            // for each line in the file
                            foreach (string s in Lines)
                            {

                                // if the definition has line numbers defined
                                if (LineNumbers[0].Length > 0)
                                {
                                    if (LineNumbers[0].Length > 1)
                                    {
                                        // if the current line is greater than the
                                        // max line number, break
                                        if (lineCounter > LineNumbers[0][1])
                                        {
                                            break;
                                        }
                                    }

                                    // if the line number is equal to or greater than the 
                                    // start line, add it to the list
                                    if (lineCounter >= LineNumbers[0][0])
                                    {
                                        lines.Add(s);
                                    }
                                }
                                else
                                {
                                    //if no line numbers are defined just return the whole file
                                    return ReadFileLines(Path, Encoding);
                                }
                            }
                        }
                        else
                        {
                            // creates an enumerable with the lines in the file
                            IEnumerable<string> Lines = GetEnumerable(Path,Encoding);

                            // for each line in the file
                            foreach (string s in Lines)
                            {

                                // for each line number definition
                                for (int i = 0; i < LineNumbers.Length; i++)
                                {
                                    // if this definition has line numbers
                                    if (LineNumbers[i].Length > 0)
                                    {
                                        // if there is a lineEnd definition
                                        if (LineNumbers[i].Length > 1)
                                        {
                                            // if the current line is greater than the
                                            // max line number, break
                                            if (lineCounter > LineNumbers[i][1])
                                            {
                                                break;
                                            }
                                        }

                                        // if the line number is equal to or larger
                                        // than the start line, add it to the list
                                        if (lineCounter >= LineNumbers[i][0])
                                        {
                                            lines.Add(s);
                                        }
                                    }
                                }
                            }
                        
                            // line counter increment
                            lineCounter++;
                        }
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
            } 
            else
            {
                // if no lines defined just return the whole file
                return ReadFileLines(Path, Encoding);
            }

            return lines.ToArray();
        }

        private static IEnumerable<string> GetEnumerable(string Path, Encoding Encoding)
        {
            if(Encoding == null)
            {
                return File.ReadLines(Path);
            }
            return File.ReadLines(Path, Encoding);
        }

    }
}
